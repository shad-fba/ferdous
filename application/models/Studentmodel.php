<?php

class Studentmodel extends CI_Model {
    
    public function __construct()
    {
         parent::__construct();
        //$this->load->helper('url'); 
    }
    // function getAll() {
    //     $q = $this->db->query("SELECT * FROM items");
    //     if($q->num_rows() >0){
    //         foreach($q->result() as $row){
    //             $data[]=$row;
    //         }
    //     }
    //     return $data;
    // }

    function create($data){
        $this->db->insert('student', $data);
        
    }
    function getInfo(){

        
        $query = $this->db->get('student');
        $res=$query->result();

        return $res;

    }
    function view($id){

        $this->db->where('id',$id);
        $query = $this->db->get('student');
        $res=$query->result();

        return $res;

    }
    function delete($id){

        
        $this->db->where('id',$id);
        $this->db->delete('student');

    }

}
?>
