<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <title>Student</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/ui/jquery-ui.min.css">
    <script src="<?php echo base_url() ?>bootstrap/js/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>bootstrap/ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>bootstrap/js/jquery.validate.min.js" type="text/javascript"></script>

    <style>
    body{overflow-x: hidden}
    </style>

    <!-- Latest compiled and minified JavaScript -->

</head>
<body>
    <div class="row clearfix">
        <div class="col-md-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student</a>
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>/StudentController/index">Registration</a></li>
                        <li><a href="<?php echo site_url(); ?>/StudentController/get">Student info</a></li> 

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">About
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Test 1-1</a></li>
                                    <li><a href="#">Test 1-2</a></li>
                                    <li><a href="#">Test 1-3</a></li> 
                                </ul>
                            </li>
                            <li><a href="#">Contact</a></li> 
                        </ul>
                    </div>
                </nav>

        </div>
    </div>
